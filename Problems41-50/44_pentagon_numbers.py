import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def pentagonalNumber(P):
    n1 = (np.sqrt(24 * P + 1) + 1) / 6
    n2 = (np.sqrt(24 * P + 1) - 1) / 6
    if (n1 % 1 == 0 or n2 % 1 == 0):
        return True
    else:
        return False

def range2():
    n = 1
    while True:
        yield n
        n += 1

P = lambda n: n * (3 * n - 1) / 2
P_sum = lambda n, m: P(n) + P(m)
P_diff = lambda n, m: abs(P(n) - P(m))

def diffNeighbors():
    for j in range2():
        for n in range2():
            if P_diff(j, j + n) > P_diff(j + 1, j + 2):
                break
            if pentagonalNumber(P_sum(j, j + n)) and pentagonalNumber(P_diff(j, j + n)):
                return P_diff(j, j + n)

print(diffNeighbors())
