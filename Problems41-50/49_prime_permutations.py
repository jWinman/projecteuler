import numpy as np
import time

# erstellt Liste mit Primzahlen
def primelist(upper_limit):
        list1 = [i for i in range(0, upper_limit + 1)]
        list1[1] = 0 # setzt Primzahl 1 = 0
        
        for k in list1:
                if (k >= 2):
                        c = (upper_limit - upper_limit % k) / k
                        n = 2
                        while (n <= c):
                                list1[k * n] = 0
                                n += 1
        return list1

def permutation(integer1, integer2):
    string_int1 = str(integer1)
    string_int2 = str(integer2)
    sortiert1 = list(map(str, np.sort([int(i) for i in string_int1])))
    sortiert2 = list(map(str, np.sort([int(i) for i in string_int2])))
    if sortiert1 != sortiert2:
        return False
    return True

def main(upper, lower):
    primes = primelist(upper)
    zeile = 0
    for number1 in primes:
        zeile += 1
        if (number1 > lower and number1 != 1487):
            for number2 in primes[zeile:]:
                if (number2 > lower):
                    number3 = number2 + abs(number1 - number2)
                    if (permutation(number1, number2) and number3 in primes and permutation(number1, number3)):
                        return np.array([number1, number2, number3])

t1 = time.clock()
print(main(10000, 1000))
print(time.clock() - t1)
