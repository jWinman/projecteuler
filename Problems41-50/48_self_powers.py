import numpy as np

def addition_self_powers(upper_limit):
    summe = 0
    for i in range(1, upper_limit+1):
        summe += i**i
    summe_str = str(summe)
    return summe_str[-10:]

print(addition_self_powers(1000))
