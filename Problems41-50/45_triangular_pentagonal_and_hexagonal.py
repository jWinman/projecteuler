import numpy as np
import time

triangle = lambda n: n * (n + 1) / 2
pentagonal = lambda n: n * (3 * n - 1) / 2
hexagonal = lambda n: n * (2 * n - 1)

Pentagonal = []
Hexagonal = []

boolian = True
n = 2

t1 = time.clock()
while (boolian):
        Pentagonal.append(pentagonal(n))
        Hexagonal.append(hexagonal(n))
        t = triangle(n)

        if (t in Pentagonal and t in Hexagonal and n != 285):
                break
        n += 1

print(triangle(n))
print(t1 - time.clock())
