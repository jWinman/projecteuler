import numpy as np

# erstellt Liste mit Primzahlen
def primelist(upper_limit):
        list1 = [i for i in range(0, upper_limit + 1)]
        list1[1] = 0 # setzt Primzahl 1 = 0
        
        for k in list1:
                if (k >= 2):
                        c = (upper_limit - upper_limit % k) / k
                        n = 2
                        while (n <= c):
                                list1[k * n] = 0
                                n += 1
        return list1

def check_conjecture(number):
    primes = primelist(number)
    if (number in primes):
        return 2
    for prime in primes:
        if (prime > 0):
            squareNumber = np.sqrt((number - prime) / 2)
            if squareNumber % 1 == 0:
                return 0
    return 1

i = 3
while (check_conjecture(i) != 1):
    i += 2
print(i)
