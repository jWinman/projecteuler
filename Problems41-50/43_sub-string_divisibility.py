import numpy as np

def permutation_gen(numberstr):
    new_str = numberstr
    right = 0
    while (new_str != numberstr[::-1]):
        pivot = longestSeq(new_str) - 1
        right = rightmostSucc(new_str, pivot)
        if (right == -1):
            new_str = new_str[:pivot] + new_str[right] + new_str[pivot+1:right] + new_str[pivot]
        else:
            new_str = new_str[:pivot] + new_str[right] + new_str[pivot+1:right] + new_str[pivot] + new_str[right+1:]

        new_str = new_str[:pivot + 1] + new_str[pivot+1:][::-1]
        yield new_str


def longestSeq(numberstr):
    i = -1
    while (i > -len(numberstr) and numberstr[i] < numberstr[i - 1]):
        i -= 1
    return i

def rightmostSucc(numberstr, pivot):
    i = -1
    while (numberstr[i] < numberstr[pivot]):
        i -= 1
    return i

def primeDivisibility(numberstr):
    if (int(numberstr[1] + numberstr[2] + numberstr[3]) % 2 != 0):
        return False
    if (int(numberstr[2] + numberstr[3] + numberstr[4]) % 3 != 0):
        return False
    if (int(numberstr[3] + numberstr[4] + numberstr[5]) % 5 != 0):
        return False
    if (int(numberstr[4] + numberstr[5] + numberstr[6]) % 7 != 0):
        return False
    if (int(numberstr[5] + numberstr[6] + numberstr[7]) % 11 != 0):
        return False
    if (int(numberstr[6] + numberstr[7] + numberstr[8]) % 13 != 0):
        return False
    if (int(numberstr[7] + numberstr[8] + numberstr[9]) % 17 != 0):
        return False
    return True


SumPanDigits = 0
for permutation in permutation_gen("0123456789"):
    if primeDivisibility(permutation):
        print(permutation)
        SumPanDigits += int(permutation)

print(SumPanDigits)

