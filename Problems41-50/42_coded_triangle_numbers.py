import numpy as np

def sumOfWord(word):
    return sum([ord(i) - ord('A') + 1 for i in word[1:-1]])

def triangleNumber(t):
    n1 = 0.5 * (1 + np.sqrt(1 + 8 * t))
    n2 = 0.5 * (1 - np.sqrt(1 + 8 * t))
    if (n1 % 1 == 0 or n2 % 1 == 0):
        return True
    else:
        return False

def triangleCounter(wordList):
    counter = 0
    for word in wordList:
        if triangleNumber(sumOfWord(word)):
            counter += 1
    return counter

with open("words.txt", "r") as words:
    words = words.read()
    words = words.split(",")
    counter = triangleCounter(words)

print(counter)
