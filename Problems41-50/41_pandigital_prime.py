import numpy as np
import itertools as it
import time

def prime(number):
    if (number == 2):
        return True
    upper_limit = int(np.ceil(np.sqrt(number)))
    for i in range(2, upper_limit + 1):
        if (number % i == 0):
            return False
    return True

pandigital = "987654321"

def biggest_prime_pan(pandigital):
    while (len(pandigital) > 0):
        for i in it.permutations(pandigital):
            number = int("".join(i))
            if (prime(number)):
                return number
        pandigital = pandigital[1:]

print(biggest_prime_pan(pandigital))
