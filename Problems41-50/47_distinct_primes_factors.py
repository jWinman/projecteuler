import numpy as np
import sympy

number = 4

def distinct_primes(number):
    counter = 0
    i = 1
    while (counter != number):
        if (len(sympy.factorint(i)) == number):
            counter += 1
        else:
            counter = 0
        i += 1
    return i - number

print(distinct_primes(number))
