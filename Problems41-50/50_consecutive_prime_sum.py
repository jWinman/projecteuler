import numpy as np
import time

def primelist(upper_limit):
# erstellt Primzahl-array bis upper_limit
        list1 = [i for i in range(0, upper_limit + 1)]
        list1[1] = 0

        for k in list1:
                if (k >= 2):
                        c = (upper_limit - upper_limit % k) / k
                        n = 2
                        while (n <= c):
                                list1[k * n] = 0
                                n += 1
        return list1

def find_prime(sum_primes, list1, list2):
#erstellt entarteten binären Baum und sucht darin nach Primzahl
#Ergebnis ist die gesucht Primzahl
    upper_list = [sum_primes]
    lower_list = []
    left_array = []
    right_array = []

    m = 0
    n = -1
    while (True):
        for i in lower_list:
            if (i == list2[i]):
                return i

        while (list1[m] == 0):
            m += 1
        left_array.append(list1[m])
        m += 1

        while (list1[n] == 0):
            n -= 1
        right_array.append(list1[n])
        n -= 1

        lower_list = []

        k = 0
        l = -1
        for i in upper_list:
            if ((i - left_array[l]) not in lower_list):
                lower_list.append(i - left_array[l])
            lower_list.append(i - right_array[k])
            k += 1
            l -= 1

        upper_list = lower_list

def highest_prime(upper_limit):
#löscht alle Primzahl die zu groß sind
#und führt dann find_prime() aus
    primes = primelist(upper_limit)
    sum_primes = sum(primes)

    i = -1
    while (sum_primes > upper_limit):
        sum_primes -= primes[i]
        i -= 1
    
    sum_primes = find_prime(sum_primes, primes[:i], primes[:sum_primes])
    return sum_primes

t = time.clock()
print(highest_prime(1000000))
print(time.clock() - t)
