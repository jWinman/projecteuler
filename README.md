# Solutions for ProjectEuler #

# Required programs #
* Python 2.7 or higher

# Python libraries #
* numpy 1.7.1
* scipy 0.12.0