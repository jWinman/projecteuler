import numpy as np
from time import *

a = open("triangle.txt").readlines()
b = []

for i in range(len(a)):
        b.append(a[i].split())

for i in range(len(b)):
        for j in range(len(b[i])):
                b[i][j] = int(b[i][j])

length = len(b) - 1

def dynamic_prog(array2row):
        for i in range(len(array2row[0])):
                tmp1 = array2row[0][i] + array2row[1][i]
                tmp2 = array2row[0][i] + array2row[1][i + 1]
                if (tmp1 > tmp2):
                        array2row[0][i] = tmp1
                else:
                        array2row[0][i] = tmp2

for j in range(length):
       j = length - 1 - j
       dynamic_prog(b[j:])
       del b[j + 1]

print(b)
