import numpy as np

coins = np.array([1, 2, 5, 10, 20, 50, 100, 200])
target = 200

combinedCoins = np.ones((target, len(coins)))

for i in range(2, target + 1):
    for j in range(1, len(coins)):
        rest = i - coins[j]
        if (rest < 0):
            combinedCoins[i - 1][j:] = np.full(len(coins) - j, combinedCoins[i - 1][j - 1])
            break
        elif (rest == 0):
            combinedCoins[i - 1][j:] = np.full(len(coins) - j, combinedCoins[i - 1][j - 1] + 1)
            break
        elif (rest > 0):
            combinedCoins[i - 1][j] = combinedCoins[i - 1][j - 1] + combinedCoins[rest - 1][j]

print(combinedCoins[-1,-1])

