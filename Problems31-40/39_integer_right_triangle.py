import numpy as np

def p(a, b):
        return a + b + np.sqrt(a**2 + b**2)

def triangle(p_upper):
        p_array = np.zeros(p_upper)
        for i in range(p_upper):
                for j in range(p_upper):
                        p_ij = p(i, j)
                        if (p_ij > p_upper):
                                break
                        if (p_ij % 1 != 0):
                                continue
                        p_array[p_ij - 1] += 1
        return p_array

triangle = triangle(1000)
maximum = max(triangle)

for i in range(len(triangle)):
        if (triangle[i] == maximum):
                print(i + 1)
