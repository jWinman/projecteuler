import numpy as np
from math import *
from random import *
from time import *

def factorize(a):
        str_a = str(a)
        summe = 0
        for i in range(len(str_a)):
                summe += factorial(int(str_a[i]))
        return summe

def summe_factorize_factorial(upper_limit):
        factorize_array = []
        for i in range(3, upper_limit):
                j = 0
                boolian = True
                while (j < 10):
                        j += 1
                        if (i < factorial(j) and str(j) in str(i)):
                                boolian = False
                                break

                if (boolian):
                        factorize_array.append(i)
        summe = 0
        for i in factorize_array:
                factorize_i = factorize(i)
                if (i == factorize_i):
                        summe += i
        return summe

def brute_force(upper_limit):
        summe = 0
        for i in range(3, upper_limit):
                factorize_i = factorize(i)
                if (i == factorize_i):
                        summe += i
        return summe

upper_limit = 3265920 # = 9! * 7 Begründung siehe Plot!

t = clock()
print(brute_force(upper_limit))
print(clock() - t)
