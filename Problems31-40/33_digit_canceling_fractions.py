import numpy as np
from time import *

fractions = np.zeros(4)
index = 0

for i in range(11, 100):
        a = str(i)
        if (i % 10 == 0):
                continue
        for j in range(11, i):
                b = str(j)
                if a[0] in b:
                        if ((a[0] == b[0] and int(b[1]) / int(a[1]) == j / i) or (a[0] == b[1] and int(b[0]) / int(a[1]) == j / i)):
                                fractions[index] = j / i
                                index += 1
                if a[1] in b:
                        if ((a[1] == b[0] and int(b[1]) / int(a[0]) == j / i) or (a[1] == b[1] and int(b[0]) / int(a[0]) == j / i)):
                                fractions[index] = i / j
                                index += 1

def product(array):
        product = 1
        for i in array:
                product *= i
        return product

print(product(fractions))

