import numpy as np
import itertools as it

all_perms = it.permutations("987654321")

def ConcatProd(number):
    for i in range(1, len(number) // 2 + 1):
        first, last = number[:i], number[i:]
        next = number[i:len(first) + i]

        n, j = 2, 0
        while (True):
            nextConjecture = int(first) * n
            if (nextConjecture == int(next)):
                j += len(next)
                n += 1
                next = last[j:2 * j]
            elif (nextConjecture > int(next) and j != len(last) - len(next)):
                next = last[:len(next) + 1]
            else:
                break;

            if (j == len(last)):
                return True
    return False

for i in all_perms:
    if (ConcatProd("".join(i))):
        print("".join(i))
        break
