import numpy as np
import time

def primelist(upper_limit):
        list1 = [i for i in range(0, upper_limit + 1)]
        list1[1] = 0
        
        for k in list1:
                if (k >= 2):
                        c = (upper_limit - upper_limit % k) / k
                        n = 2
                        while (n <= c):
                                list1[k * n] = 0
                                n += 1
        return list1

def one_rotation(string):
        string2 = string[0]
        string2 = string[1:] + string2
        return string2

def rotation(string, primelist):
        counter_local = 0
        for j in range(len(string)):
                string1 = string
                string = one_rotation(string)
                if (string1 == string):
                        return 1
                elif (int(string) == primelist[int(string)]):
                        primelist[int(string)] = 0
                        counter_local += 1
                else:
                        return 0
        return counter_local

def circular_primes(primelist):
        counter = 4
        for i in primelist:
                i = str(i)
                if (len(i) >= 2):
                        counter += rotation(i, primelist) 

        return counter

t2 = time.clock()
print(circular_primes(primelist(1000000)))
print(time.clock() - t2)
