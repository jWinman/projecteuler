import numpy as np
import itertools as it

all_perm = it.permutations("123456789")

def produkt(number, resultSet):
    for i in range(1, len(number) // 3):
        for j in range(i + 1, len(number) // 3 * 2):
            fac1 = number[0:i]
            fac2 = number[i:j]
            prod = number[j:]
            prodFac = str(int(fac1) * int(fac2))
            if (prod == prodFac):
                resultSet.add(int(prod))

resultSet = set()
for j, i in enumerate(all_perm):
    produkt("".join(i), resultSet)

print(sum(list(resultSet)))
