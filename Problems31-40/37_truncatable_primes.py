import numpy as np
import time

def prime(number):
    if (number == 1):
        return True
    if (number == 2):
        return True
    upper_limit = int(np.ceil(np.sqrt(number)))
    for i in range(2, upper_limit + 1):
        if (number % i == 0):
            return False
    return True

# truncatable from right to left
def tertiary_tree(root, empty_list):
    if (prime(root)):
        empty_list.append(root)
        d = int(str(root) + "1")
        a = int(str(root) + "3")
        b = int(str(root) + "7")
        c = int(str(root) + "9")
        tertiary_tree(d, empty_list)
        tertiary_tree(a, empty_list)
        tertiary_tree(b, empty_list)
        tertiary_tree(c, empty_list)
    else:
        return

# truncatable from left to right
def remove_left(list1):
    for i in range(len(list1)):
        j = str(list1[i])
        while (len(j) > 0 and prime(int(j))):
            j = j[1:]
        if (len(j) > 0):
            list1[i] = 0
        elif (str(list1[i])[-1] == "1" or list1[i] < 10):
            list1[i] = 0

def remove_zeros(list1):
    i = 0
    while (0 in list1):
        list1.remove(0)

prime_tree1 = []
prime_tree2 = []
prime_tree3 = []
prime_tree5 = []
prime_tree7 = []
#tertiary_tree(1, prime_tree1)
#remove_left(prime_tree1)
tertiary_tree(2, prime_tree2)
remove_left(prime_tree2)
tertiary_tree(3, prime_tree3)
remove_left(prime_tree3)
tertiary_tree(5, prime_tree5)
remove_left(prime_tree5)
tertiary_tree(7, prime_tree7)
remove_left(prime_tree7)

#remove_zeros(prime_tree1)
remove_zeros(prime_tree2)
remove_zeros(prime_tree3)
remove_zeros(prime_tree5)
remove_zeros(prime_tree7)



prime_tree = prime_tree1 + prime_tree2 + prime_tree3 + prime_tree5 + prime_tree7
print(prime_tree, len(prime_tree), sum(prime_tree))
