import numpy as np
from time import *

def palindrome(number_str):
        if (len(number_str) == 0):
                return True
        elif (number_str[0] == number_str[- 1]):
                return palindrome(number_str[1:-1])
        elif (number_str[0] != number_str[-1]):
                return False

def binary(number):
        number_bin = str(number % 2)
        while (number // 2 != 0):
                number //= 2
                number_bin = str(number % 2) + number_bin
        return number_bin
                
def sum_palindrome(upper_limit):
        summe = 0
        for i in range(int(upper_limit)):
                if (palindrome(str(i))):
                        tmp_bin = binary(i)
                        if (palindrome(tmp_bin)):
                                summe += i
        return summe

print(sum_palindrome(1e6))
