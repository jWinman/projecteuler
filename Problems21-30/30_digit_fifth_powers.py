import numpy as np
from math import *
from random import *
from time import *

def power(a, b):
        str_a = str(a)
        summe = 0
        for i in range(len(str_a)):
                summe += int(str_a[i])**b
        return summe

def brute_force(upper_limit, b):
        summe = 0
        for i in range(3, upper_limit):
                factorize_i = power(i, b)
                if (i == factorize_i):
                        summe += i
        return summe

b = 5
upper_limit = 9**6 # Begründung siehe Abbildung

t = clock()
print(brute_force(upper_limit, b))
print(clock() - t)
