import numpy as np

def sum_of_spiral(length):
        summe = 4
        difference = 2
        number = 3
        counter = 1
        while (difference + 1 <= length):
                number += difference
                summe += number
                counter += 1
                if (counter == 4):
                        counter = 0
                        difference += 2
        return summe

print(sum_of_spiral(1001))



