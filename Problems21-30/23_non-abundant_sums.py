import numpy as np
from time import *

# testet ob n eine abundant number ist
def abundant(n):
        summe = 1
        teiler = 0
        for i in range(2, int(n / 2) + 1):
                if (summe > n):
                        return True
                elif (i == teiler):
                        return False
                elif (n % i == 0):
                        summe += i
                        teiler = n // i
                        if (teiler != i):
                                summe += teiler
                        elif (summe > n):
                            return True
                        else:
                            return False

#def abundant(n):
#    summe = 1
#    for i in range(2, int(n / 2) + 1):
#        if (summe > n):
#            return True
#        elif (n % i == 0):
#            summe += n // i
#    if (summe <= n):
#        return False
#    else:
#        return True

# gibt eine Liste zurück mit allen Zahlen, die sich NICHT als Summe zweier abundant numbers darstellen lassen
def two_abundant(upper_limit, list_of_abundant):
    list_of_all = np.arange(upper_limit)
    counter = 0
    for i in list_of_abundant:
        for j in list_of_abundant[counter:]:
            if ((i + j) < upper_limit):
                list_of_all[i + j] = 0
            else:
                break
        counter += 1
    return list_of_all

abundant_list = lambda lower_limit, upper_limit: [i for i in range(lower_limit, upper_limit) if abundant(i)]

upper_limit =28123
lower_limit = 12


t1 = clock()
abundantList = abundant_list(lower_limit, upper_limit)
print(sum(two_abundant(upper_limit, abundantList)))
print(t1 - clock())
