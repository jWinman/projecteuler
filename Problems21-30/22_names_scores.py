import numpy as np
from time import *

with open("names.txt", "r") as fobj:
        fobj = fobj.read()
        fobj1 = fobj.split(",")
        fobj1.sort()

        summe = 0

        for i in range(len(fobj1)):
                j = 1
                summe_tmp = 0
                while (fobj1[i][j] != '"'):
                        summe_tmp += ord(fobj1[i][j]) - ord('A') + 1
                        j += 1
                summe += (i + 1) * summe_tmp

print(summe)
