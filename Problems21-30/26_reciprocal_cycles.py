import numpy as np

def cycle(a):
        i = 10
        liste = [i]
        while (True):
                i = (i - int(i / a) * a) * 10
                if (i == 0):
                        return 0
                if i in liste:
                        for j in range(len(liste)):
                                if (liste[j] == i):
                                        return len(liste) - j
                liste.append(i)

array = np.zeros(1001)

for i in range(1, len(array) + 1):
        array[i - 1] = cycle(i)

maximum = max(array)
print(maximum)

for i in range(len(array)):
        if (array[i] == maximum):
                print(i + 1)
