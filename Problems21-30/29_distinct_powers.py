import numpy as np
from time import *

def integer_combinations(a):
        array = np.zeros((a - 1, a - 1, 2))
        for i in range(a - 1):
                power_i = power_test(i + 2)
                for j in range(a - 1):
                        if (power_i == 0):
                                array[i][j] = (i + 2, j + 2)
                        else:
                                new_power_ij = (power_i[0], power_i[1] * (j + 2))
                                array[i][j] = new_power_ij
        return array

def power_test(number):
        counter = [0]
        if (number == 2):
                return 0
        for i in range(2, int(np.ceil(np.sqrt(number))) + 1):
                counter1 = 0
                while (number % i == 0):
                        number //= i
                        counter1 += 1
                        if (counter[-1] != 0):
                                return 0
                if (counter1 > 0):
                        counter.append((i, counter1))
        return counter[-1]

def count_in_array(a, array):
        for i in range(len(array)):
                if a in array[i]:
                        return True
        return False

def anzahl(array):
        anzahl = 0
        for i in range(len(array)):
                for j in range(len(array)):
                        if (count_in_array(array[i][j], array[:i])):
                                anzahl += 1

        return anzahl

def brute_force(a):
        liste = []
        counter = 0
        for i in range(a - 1):
                for j in range(a - 1):
                        if ((i + 2)**(j + 2) in liste):
                                counter += 1
                        liste.append((i + 2)**(j + 2))
        return counter

upper_limit = 100

print((upper_limit - 1)**2 - anzahl(integer_combinations(upper_limit).tolist()))
print((upper_limit - 1)**2 - brute_force(upper_limit))
