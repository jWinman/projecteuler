import numpy as np
import functools as ft

def primelist(upper_limit):
    list1 = np.arange(upper_limit + 1)
    list1[1] = 0

    for k in list1:
        if (k >= 2):
            c = (upper_limit - upper_limit % k) / k
            n = 2
            while (n <= c):
                list1[k * n] = 0
                n += 1
    return list1[list1 != 0]

def factorGen(root):
    k = 0
    factor1 = 5
    factor2 = 7
    while (factor2 <= root):
        yield factor1, factor2
        k += 1
        factor1 = 6 * k - 1
        factor2 = 6 * k + 1


def isPrime(number):
    root = np.ceil(np.sqrt(number))
    if (number % 2 == 0 or number % 3 == 0):
        if (number == 2 or number == 3):
            return True
        else:
            return False
    else:
        for factor1, factor2 in factorGen(root):
            if (number % factor1 == 0 or number % factor2 == 0):
                return False
        return True

quadratic = lambda n, a, b: n**2 + a * n + b
primes = primelist(1000)
maximum = (0, 0)

for i in primes:
    for j in primes:
        b = i
        a = j - i - 1
        quadraticLocal = ft.partial(quadratic, b=b, a=a)
        n = 2
        posPrime = quadraticLocal(n)
        while (posPrime > 0 and isPrime(posPrime)):
            n += 1
            posPrime = quadraticLocal(n)

        if (maximum[0] < n):
            maximum = (n, a * b)

print(maximum)
