import numpy as np
import itertools as it
import time

t1 = time.clock()
numberstr = "0123456789"
j = 0
for perm in it.permutations(numberstr, len(numberstr)):
    j += 1
    if (j == 1e6):
        print(j, perm)
        break;
t2 = time.clock()
print(t2-t1)


def permutation_gen(numberstr):
    new_str = numberstr
    while (True):
        pivot = longestSeq(new_str) - 1
        right = rightmostSucc(new_str, pivot)
        if (right == -1):
            new_str = new_str[:pivot] + new_str[right] + new_str[pivot+1:right] + new_str[pivot]
        else:
            new_str = new_str[:pivot] + new_str[right] + new_str[pivot+1:right] + new_str[pivot] + new_str[right+1:]

        new_str = new_str[:pivot + 1] + new_str[pivot+1:][::-1]
        yield new_str


def longestSeq(numberstr):
    i = -1
    while (numberstr[i] < numberstr[i - 1] and i > -len(numberstr)):
        i -= 1
    return i

def rightmostSucc(numberstr, pivot):
    i = -1
    while (numberstr[i] < numberstr[pivot]):
        i -= 1
    return i

j = 2
for permutation in permutation_gen("0123456789"):
    if (j == 1e6):
        print(j, permutation)
        break;
    j += 1
print(time.clock() - t2)
