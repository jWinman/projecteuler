import numpy as np
from time import *

def d(n):
        summe = 1
        teiler = 0
        for i in range(2, int(n / 2) + 1):
                if (i == teiler):
                        break
                if (n % i == 0):
                        summe += i
                        teiler = n // i
                        if (teiler != i):
                                summe += teiler
        return summe

def summe_amicable_numbers(N_limit):
        sum_amicable = 0
        for a in range(1, N_limit):
                b = d(a)
                tmp = d(b)
                if (tmp == a and b != a):
                        sum_amicable += a
        return sum_amicable

t = clock()
#print(summe_amicable_numbers(10000))
print(clock() - t)
print(d(4))
