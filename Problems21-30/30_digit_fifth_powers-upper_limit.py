import numpy as np
import matplotlib.pylab as plt
from math import *
from random import *

def power(a, b):
        str_a = str(a)
        summe = 0
        for i in range(len(str_a)):
                summe += int(str_a[i])**b
        return summe

neuner = [9]
for i in range(1, 9):
        neuner.append(neuner[i - 1] + 9 * 10**i)

random = [randint(1, neuner[-7]) for i in range(1000)]

x = np.array(neuner)
y = np.array([len(str(power(i, 5))) for i in x])
x_length = np.array([len(str(i)) for i in x])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, y, "b.", label="len(factorize(i))")
ax.plot(x, x_length, "r.", label="len(i)")
ax.set_xscale("log")
ax.legend(loc="best")
fig.savefig("30_digit_fifth_power-upper_limit.pdf")
