from intro import *

def squarsumdiff(x):
    sumsquare = 0
    i = 1
    while i<=x:
        sumsquare = sumsquare + i**2
        i = i+1
    sum = x**2/4*(x+1)**2 - sumsquare
    return sum

print(squarsumdiff(100))
        
