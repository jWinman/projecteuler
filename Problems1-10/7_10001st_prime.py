from intro import *

def prime(a, q):
	b = int(ceil(sqrt(a)))
	j = 0
	while q[j] <= b:
		lenge = j + 1
		j = j + 1

	z=[]
	for x in q[:lenge]:
		for i in range(0, len(q[:lenge])):
			if a % x == 0:
				z.append(1)
	if len(z) == 0:
		return 1
	else:
		return 0
	


def primecounter(anzahl):
	a = []
	a.append(2)
	a.append(3)
	for i in range(2, anzahl):
		a.append(a[i-1] + 2)
		while prime(a[i], a) == 0:
			a[i] = a[i] + 2
	return a[-1]

print(primecounter(10001))

