 #!/usr/bin/python
 #coding=utf8
from __future__ import division
from numpy import *
from time import *

def primelist(upper_limit):
        list1 = [i for i in range(0, upper_limit + 1)]
        list1[1] = 0
        
        for k in list1:
                if (k >= 2):
                        c = (upper_limit - upper_limit % k) / k
                        n = 2
                        while (n <= c):
                                list1[k * n] = 0
                                n += 1
        return list1

print(sum(primelist(2000000)))
