from intro import *

liste = ["z", "b", "c","t", "a", "e", "c"]

def qsort1(list):
   if list == []: 
        return []
   else:
       pivot = list[0]
       lesser = qsort1([x for x in list[1:] if x < pivot])
       greater = qsort1([x for x in list[1:] if x >= pivot])
       return lesser + [pivot] + greater
t = clock()
print(qsort1(liste))
print(clock() - t)
