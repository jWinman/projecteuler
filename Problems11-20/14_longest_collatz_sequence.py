from intro import *

length = 1000000
zahlenkette = zeros(length + 1)

def f(n):
        if (n % 2 == 1):
                return 3 * n + 1
        else:
                return n / 2

def length_of_chain(n, counter):
        while (n > 1):
                n = f(n)
                counter += 1
        return counter

for j in range(length + 1):
        zahlenkette[j] = length_of_chain(j, 0)

answer = max(zahlenkette)

for j in range(length + 1):
        if (zahlenkette[j] == answer):
                print(j)

quit()
# Funktion die Zahlen generiert
# n = Startzahl
# counter = Anzahl an Schritte
# array ist übergebenes Array mit Nullen
def f(n, i, counter, array):
        while (i < len(array) and array[i] != 0):
                i += 1

        if (i == len(array) or n > length * len(array)):
            return;

        # inverse Funktionsvorschrift mit n ausführen
        n1 = (n - 1) / 3.0
        n2 = 2 * n
        counter += 1

        if (n1 % 2 == 1 and n1 > 1):
                if (len(array) > n1):
                        array[n1] = counter
                
                f(n1, i, counter, array)

        if (len(array) > n2):
                array[n2] = counter

        f(n2, i, counter, array)

f(2, 2, 0, zahlenkette)
answer = max(zahlenkette)

for j in arange(length):
        if (zahlenkette[j] == 0):
                print("Nuller: ")
                print(j)
        if (zahlenkette[j] == answer):
                print("Maximum: ")
                print(j)
