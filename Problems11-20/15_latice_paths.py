import numpy as np
from time import *

t = clock()
length = 40
a = np.zeros((length)**2).reshape((length, length))

for i in range(length):
        a[0][i] = 1
        for j in range(1, length):
                if (i == 0):
                        a[j][i] = a[j - 1][i]
                elif (i < length - 1):
                        counter = i
                        while (counter >= 0):
                                a[j][i] += a[j - 1][counter]
                                counter -= 1
                if (i == length - 1):
                        a[j][i] = a[j][i - 1]

print(2 * (sum(a[:,-1]) + sum(a[-1,:]) - a[-1][-1]))
print(clock() - t)
