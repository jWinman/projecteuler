import numpy as np

feb1 = 28
feb2 = 29
jan = 31
april = 30

number_days = (4 * 30 + 7 * 31 + 28) + 100 // 4

year = np.zeros(12*31).reshape((12, 31))

century = [year for i in range(101)]

century = np.array(century)

count = 1
for i in range(len(century)):
        for j in range(len(century[i])):
                if (j == 1):
                        if (i > 0 and i % 4 == 0):
                                for k in range(feb2):
                                        count += 1
                                        if (count % 7 == 0 and k == 0):
                                                century[i][j][k] = 1
                        else:
                                for k in range(feb1):
                                        count += 1
                                        if (count % 7 == 0 and k == 0):
                                                century[i][j][k] = 1

                elif ((j % 2 == 0 and j < 7) or (j % 2 == 1 and j >= 7)):
                        for k in range(jan):
                                count += 1
                                if (count % 7 == 0 and k == 0):
                                        century[i][j][k] = 1

                elif ((j % 2 == 1 and j < 7) or (j % 2 == 0 and j >= 7)):
                        for k in range(april):
                                count += 1
                                if (count % 7 == 0 and k == 0):
                                        century[i][j][k] = 1
                                
answer = sum(sum(sum(century))) - sum(sum(century[0]))

print(answer)
