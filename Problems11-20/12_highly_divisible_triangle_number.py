from intro import *

divisor = 500

n=10000.0
a=0
i=0
t1=clock()
while i+1<=divisor:
	n += 1
	a=n/2*(n+1)
	b=int(ceil(sqrt(a)))
	j=1
	i=0
	while j<b:
		if a%j==0:
			i += 1
		j += 1
	
	i *= 2
t2=clock()
print n,i,a,t2-t1
