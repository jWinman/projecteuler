import numpy as np
import sys

# berechnet sqrt(2) = 1 / (2 + 1 / (2 + 1 / 2 + ...))
# und gibt Nominator, Denominator zurück
def sqrt_convergents(nominator, denominator, iteration):
    if (iteration > 1):
        denominator, nominator = 2 * denominator + nominator, denominator
    else:
        nominator = denominator + nominator

    iteration -= 1

    if (iteration > 0):
        return sqrt_convergents(nominator, denominator, iteration)
    else:
        return nominator, denominator

# zählt wie oft len(nominator) > len(denominator) ist
iterations = 1000
sys.setrecursionlimit(iterations+1)
counter = 0
for iteration in range(iterations):
    nominator, denominator = sqrt_convergents(1, 2, iteration)
    if len(str(nominator)) > len(str(denominator)):
        counter += 1
print(counter)

