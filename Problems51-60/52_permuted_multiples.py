import numpy as np
import time

def permutation(integers):
    string_ints = []
    string_int1 = str(integers[0])
    sortiert1 = list(map(str, np.sort([int(i) for i in string_int1])))

    for integer in integers:
        string_int = str(integer)
        sortiert = list(map(str, np.sort([int(i) for i in string_int])))
        if sortiert1 != sortiert:
            return False
    return True

def main(number_of_multiplies):
    i = 1
    multiply = np.array([i for i in range(1, number_of_multiplies + 1)])
    while (permutation(list(map(lambda x: x * i, multiply))) == False):
        i += 1
    return i

t = time.clock()
print(main(6))
print(time.clock() - t)
