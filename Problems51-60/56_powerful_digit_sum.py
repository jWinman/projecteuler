import numpy as np

def sumIntToList(number):
    string = str(number)
    return sum([int(i) for i in string])

upper_limit = 100
power = np.zeros((upper_limit, upper_limit))
for i in range(1, upper_limit):
    for j in range(1, upper_limit):
        power[i][j] = sumIntToList(i ** j)

print(max(power.flatten()))
