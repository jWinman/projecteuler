# brute force funktioniert
import math
import numpy as np

C = lambda n, r: math.factorial(n) / (math.factorial(r) * math.factorial(n - r))

def binomial(n, k):
    if (k == 0):
        return 1
    elif (2 * k > n):
        ergebnis = binomial(n, n - k)
    else:
        ergebnis = n - k + 1
        for i in range(2, k):
            ergebnis *= n - k + i
            ergebnis /= i
    return ergebnis


def loops(C):
    counter = 0
    for i in range(23, 101):
        for j in range(i):
            if C(*(i,j)) > 1e6:
                counter += 1
    return counter

print(loops(binomial))


